declare interface GeneralObject {
    [key: string]: any;
}

declare interface TypedObject<T> {
    [key: string]: T;
}
