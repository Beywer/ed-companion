import * as React from 'react';
import { connect } from 'react-redux';
import { Commander } from '../../domain/Commander';
import { commanderSelector } from '../../store/gameData/commanderSelectors';

interface Props {
    commander: Commander;
}

const CommanderInfoFC: React.FC<Props> = ({ commander: { name, ship, shipName, shipIdent } }) => {
    const shipAdditional = [shipName, shipIdent].filter((str) => !!str);

    return (
        <div>
            {name}: {ship} {shipAdditional.length > 0 && `(${shipAdditional.join(', ')})`}
        </div>
    );
};

const mapStateToProps = (state: GeneralObject) => ({
    commander: commanderSelector(state),
});

export const CommanderInfo = connect(mapStateToProps)(CommanderInfoFC);
