import * as React from 'react';
import styles from './App.scss';
import { EDEvents } from '../log/EDEvents/EDEvents';
import { EventsFilter } from '../log/EventsFilter/EventsFilter';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { CommanderInfo } from '../CommanderInfo/CommanderInfo';
import { LocationInfo } from '../LocationInfo/LocationInfo';
import { ScanInfo } from '../ScanInfo/ScanInfo';

export const App: React.FC = () => (
    <div className={styles.root}>
        <Router>
            <Switch>
                <Route path="/logs">
                    <EventsFilter />
                    <EDEvents />
                </Route>
                <Route path="/">
                    <CommanderInfo />
                    <LocationInfo />
                    <ScanInfo />
                </Route>
            </Switch>
        </Router>
    </div>
);
