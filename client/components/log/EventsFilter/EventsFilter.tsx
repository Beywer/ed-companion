import * as React from 'react';
import styles from './EventsFilter.scss';
import { EDEventType } from '../../../../common/domain/EDEventType';
import { connect } from 'react-redux';
import { typesFilterSelector } from '../../../store/typesFilter/typesFilterSelectors';
import { GeneralThunkDispatch } from '../../../utils/actionTypes';
import {
    deselectAllTypes,
    selectAllTypes,
    toggleType,
} from '../../../store/typesFilter/typesFilterActions';
import { bindActionCreators } from 'redux';

interface Props {
    selectedTypes: EDEventType[];
    toggle: (evt: React.ChangeEvent<HTMLInputElement>) => void;
    selectAll: () => void;
    deselectAll: () => void;
}

const allTypes = Object.values(EDEventType).sort();

const EventsFilterFC: React.FC<Props> = ({ selectedTypes, toggle, selectAll, deselectAll }) => {
    return (
        <>
            <div>
                <button onClick={selectAll}>Select all</button>
                <button onClick={deselectAll}>Remove all</button>
            </div>
            <ul className={styles.list}>
                {allTypes.map((type) => (
                    <li key={type} className={styles.item}>
                        <label>
                            <input
                                type="checkbox"
                                value={type}
                                checked={selectedTypes.includes(type)}
                                onChange={toggle}
                            />
                            {type}
                        </label>
                    </li>
                ))}
            </ul>
        </>
    );
};

const mapStateToProps = (state: GeneralObject) => ({
    selectedTypes: typesFilterSelector(state),
});

const mapDispatchToProps = (dispatch: GeneralThunkDispatch) => ({
    toggle: (e: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(toggleType(e.target.value as EDEventType));
    },
    ...bindActionCreators(
        {
            selectAll: selectAllTypes,
            deselectAll: deselectAllTypes,
        },
        dispatch,
    ),
});

export const EventsFilter = connect(mapStateToProps, mapDispatchToProps)(EventsFilterFC);
