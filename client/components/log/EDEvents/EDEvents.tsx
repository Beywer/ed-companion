import * as React from 'react';
import { EDEvent } from '../../../../common/domain/EDEvent';
import { connect } from 'react-redux';
import { filteredEdEventsSelector } from '../../../store/edEvents/edEventSelectors';
import styles from './EDEvents.scss';

interface Props {
    edEvents: EDEvent[];
}

const EDEventsTailFC: React.FC<Props> = ({ edEvents }) => {
    return (
        <ul>
            {edEvents.map(({ id, timestamp, event, ...rest }) => (
                <li key={id} className={styles.item}>
                    <h4>
                        {timestamp} : {event}
                    </h4>
                    <div>{JSON.stringify(rest, undefined, 8)}</div>
                </li>
            ))}
        </ul>
    );
};

const mapStateToProps = (state: GeneralObject) => ({
    edEvents: filteredEdEventsSelector(state),
});

export const EDEvents = connect(mapStateToProps)(EDEventsTailFC);
