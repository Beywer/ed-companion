import * as React from 'react';
import styles from './ExternalPlayerStatus.scss';
import { SystemScanStatus } from '../../domain/SystemScanStatus';

interface Props {
    scanStatus: SystemScanStatus;
}

export const ExternalPlayerStatus: React.FC<Props> = ({ scanStatus }) => {
    const text = externalPlayerText(scanStatus);

    if (!text) {
        return null;
    }

    return <div>Уже {text}</div>;
};

function externalPlayerText(scanInfo: SystemScanStatus): string {
    const anyDiscovered = scanInfo.bodies.find((bd) => bd.discovered);
    const anyMapped = scanInfo.bodies.find((bd) => bd.mapped);

    if (anyMapped) {
        return 'картографировано';
    }
    if (anyDiscovered) {
        return 'посещено';
    }
    return '';
}
