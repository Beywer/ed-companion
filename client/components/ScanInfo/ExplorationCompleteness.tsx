import * as React from 'react';
import { SystemScanStatus } from '../../domain/SystemScanStatus';
import { SpaceBodyType } from '../../domain/SpaceBodyType';
import { ScanLevel } from '../../domain/ScanLevel';
import styles from './ExplorationCompleteness.scss';

interface Props {
    scanStatus: SystemScanStatus;
}

export const ExplorationCompleteness: React.FC<Props> = ({ scanStatus }) => {
    const { bodies, bodiesCount } = scanStatus;

    const noBelt = bodies.filter((b) => b.type !== SpaceBodyType.Belt);

    const stars = noBelt.filter((b) => b.type === SpaceBodyType.Star);
    const notStartCount = bodiesCount - stars.length;

    const notStars = noBelt.filter((b) => b.type !== SpaceBodyType.Star);
    const detailed = notStars.filter(
        (b) => b.scanLevel === ScanLevel.Detail || b.scanLevel === ScanLevel.Mapped,
    );
    const mapped = notStars.filter((b) => b.scanLevel === ScanLevel.Mapped);

    return (
        <div className={styles.status}>
            <div>Все тела обнаружены: {getProgress(noBelt.length, bodiesCount, bodiesCount)}</div>
            <div>
                Детальное сканирование: {getProgress(detailed.length, notStartCount, bodiesCount)}
            </div>
            <div>Картографирование: {getProgress(mapped.length, notStartCount, bodiesCount)}</div>
        </div>
    );
};

function getProgress(count: number, countingBodiesCount: number, bodiesCount: number): string {
    if (!bodiesCount) {
        return 'Need scan ...';
    }

    if (!countingBodiesCount) {
        return 'ОК';
    }

    const progress = count / countingBodiesCount;

    return progress === 1 ? 'ОК' : `${(progress * 100).toFixed(1)} %`;
}
