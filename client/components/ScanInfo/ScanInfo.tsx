import * as React from 'react';
import styles from './ScanInfo.scss';
import { SystemScanStatus } from '../../domain/SystemScanStatus';
import { connect } from 'react-redux';
import { currentSystemScanStatus } from '../../store/gameData/systemScanSelectors';
import { ExternalPlayerStatus } from './ExternalPlayerStatus';
import { ExplorationCompleteness } from './ExplorationCompleteness';
import cn from 'classnames';
import { ScanLevel } from '../../domain/ScanLevel';
import { SpaceBodyType } from '../../domain/SpaceBodyType';
import { SpaceBody } from '../../domain/SpaceBody';

interface Props {
    scanStatus: SystemScanStatus;
}

const ScanInfoFC: React.FC<Props> = ({ scanStatus }) => {
    const ordered = [...scanStatus.bodies];
    ordered.sort((b1, b2) => (b1.name > b2.name ? 1 : -1));

    return (
        <div>
            <br />
            <br />
            <ExternalPlayerStatus scanStatus={scanStatus} />
            <ExplorationCompleteness scanStatus={scanStatus} />

            {ordered.length > 0 && (
                <div className={styles.bodies}>
                    {ordered.map((bd) => (
                        <div
                            key={bd.name}
                            className={cn(styles.body, {
                                [styles.detail]: isScanLevel(bd, ScanLevel.Detail),
                                [styles.mapped]: isScanLevel(bd, ScanLevel.Mapped),
                                [styles.star]: bd.type === SpaceBodyType.Star,
                                [styles.belt]: bd.type === SpaceBodyType.Belt,
                            })}
                        >
                            <h4>{bd.name}</h4>
                            {bd.type} {bd.scanLevel}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

function isScanLevel(body: SpaceBody, level: ScanLevel): boolean {
    return (
        body.type !== SpaceBodyType.Star &&
        body.type !== SpaceBodyType.Belt &&
        body.scanLevel === level
    );
}

const mapStatToProps = (state: GeneralObject) => ({
    scanStatus: currentSystemScanStatus(state),
});

export const ScanInfo = connect(mapStatToProps)(ScanInfoFC);
