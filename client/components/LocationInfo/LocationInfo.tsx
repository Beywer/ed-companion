import * as React from 'react';
import styles from './LocationInfo.scss';
import { Location } from '../../domain/Location';
import { connect } from 'react-redux';
import { locationSelector } from '../../store/gameData/locationSelectors';

interface Props {
    location: Location;
}

const LocationInfoFC: React.FC<Props> = ({ location: { starSystem, body } }) => {
    return (
        <div>
            {starSystem} | {body}
        </div>
    );
};

const mapStateToProps = (state: GeneralObject) => ({
    location: locationSelector(state),
});

export const LocationInfo = connect(mapStateToProps)(LocationInfoFC);
