import { App } from 'components/App/App';
import { connect } from 'react-redux';

const mapStateToProps = (state: GeneralObject) => ({});

export const AppContainer = connect(mapStateToProps)(App);
