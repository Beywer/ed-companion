import * as React from 'react';
import { EDEvent } from '../../common/domain/EDEvent';
import { CLIENT_NEW_EVENTS } from '../../common/constants/socketEvents';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { saveEdEvents } from '../store/edEvents/edEventActions';

interface Props {
    socket: SocketIOClient.Socket;
    saveEdEvents: (events: EDEvent[]) => void;
}

const SocketFC: React.FC<Props> = ({ socket, saveEdEvents }) => {
    React.useEffect(() => {
        socket.on(CLIENT_NEW_EVENTS, saveEdEvents);
    }, [1]);

    return null;
};

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            saveEdEvents,
        },
        dispatch,
    );

export const Socket = connect(undefined, mapDispatchToProps)(SocketFC);
