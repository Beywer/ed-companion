import { AppContainer } from 'containers/AppContainer';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from 'store/configureStore';
import 'styles/app.scss';
import { Socket } from './containers/Socket';
import { initSocket } from './utils/socket';

const root = document.getElementById('root');
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <AppContainer />
        <Socket socket={initSocket()} />
    </Provider>,
    root,
);
