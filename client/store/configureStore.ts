import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { rootReducer } from 'store/rootReducer';
import { cacheTypesFiltersMiddleware } from './typesFilter/typesFilterMiddlewares';

export function configureStore(): Store {
    return createStore(
        rootReducer,
        undefined,
        composeWithDevTools(applyMiddleware(thunk, cacheTypesFiltersMiddleware)),
    );
}
