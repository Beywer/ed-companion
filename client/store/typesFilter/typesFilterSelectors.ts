import { typesFilterPath } from '../rootPaths';
import { EDEventType } from '../../../common/domain/EDEventType';

export function typesFilterSelector(state: { [typesFilterPath]?: EDEventType[] }): EDEventType[] {
    return state[typesFilterPath] || [];
}
