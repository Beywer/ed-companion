import { Action, Dispatch, Store } from 'redux';
import { typesFilterActions } from './typesFilterActions';
import { typesFilterSelector } from './typesFilterSelectors';

export const cacheTypesFiltersMiddleware = (store: Store) => (next: Dispatch) => (
    action: Action,
) => {
    const result = next(action);
    if (typesFilterActions.includes(action.type)) {
        const selectedTypes = typesFilterSelector(store.getState());
        localStorage.setItem('typesFilter', JSON.stringify(selectedTypes));
    }
    return result;
};
