import { EDEventType } from '../../../common/domain/EDEventType';
import { PayloadedAction } from '../../utils/actionTypes';
import { Action } from 'redux';

export const TOGGLE_TYPE = 'TYPES_FILTER/TOGGLE';
export const SELECT_ALL_TYPES = 'TYPES_FILTER/SELECT_ALL_TYPES';
export const DESELECT_ALL_TYPES = 'TYPES_FILTER/DESELECT_ALL_TYPES';

export const typesFilterActions = [TOGGLE_TYPE, SELECT_ALL_TYPES, DESELECT_ALL_TYPES];

export function toggleType(type: EDEventType): PayloadedAction<EDEventType> {
    return { type: TOGGLE_TYPE, payload: type };
}

export function selectAllTypes(): Action {
    return { type: SELECT_ALL_TYPES };
}

export function deselectAllTypes(): Action {
    return { type: DESELECT_ALL_TYPES };
}
