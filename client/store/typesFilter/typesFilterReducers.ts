import { EDEventType } from '../../../common/domain/EDEventType';
import { AnyAction } from 'redux';
import { DESELECT_ALL_TYPES, SELECT_ALL_TYPES, TOGGLE_TYPE } from './typesFilterActions';
import { PayloadedAction } from '../../utils/actionTypes';

const savedFilter = localStorage.getItem('typesFilter');
const initialState: EDEventType[] = savedFilter
    ? JSON.parse(savedFilter)
    : Object.values(EDEventType);

export function typesFilterReducer(
    state: EDEventType[] = initialState,
    action: AnyAction,
): EDEventType[] {
    switch (action.type) {
        case TOGGLE_TYPE: {
            const { payload } = action as PayloadedAction<EDEventType>;

            if (state.includes(payload)) {
                return state.filter((type) => type !== payload);
            } else {
                return [...state, payload];
            }
        }
        case SELECT_ALL_TYPES: {
            return Object.values(EDEventType);
        }
        case DESELECT_ALL_TYPES: {
            return [];
        }
        default: {
            return state;
        }
    }
}
