import { edEventsPath } from '../rootPaths';
import { createSelector } from 'reselect';
import { typesFilterSelector } from '../typesFilter/typesFilterSelectors';
import { EDEventsStore } from '../../domain/EDEventsStore';
import { EDEvent } from '../../../common/domain/EDEvent';

function storedEdEventsSelector(state: { [edEventsPath]?: EDEventsStore }): EDEventsStore {
    return state[edEventsPath] || { events: {}, order: [] };
}

export function edEventsByIdSelector(state: {
    [edEventsPath]?: EDEventsStore;
}): TypedObject<EDEvent> {
    return storedEdEventsSelector(state).events;
}

export const edEventsSelector = createSelector([storedEdEventsSelector], (stored) =>
    stored.order.map((id) => stored.events[id]),
);

export const filteredEdEventsSelector = createSelector(
    [edEventsSelector, typesFilterSelector],
    (edEvents, types) => edEvents.filter(({ event }) => types.includes(event)).reverse(),
);
