import { EDEvent } from '../../../common/domain/EDEvent';
import { AnyAction } from 'redux';
import { SAVE_ED_EVENTS } from './edEventActions';
import { PayloadedAction } from '../../utils/actionTypes';
import { EDEventsStore } from '../../domain/EDEventsStore';
import produce from 'immer';

const initialState: EDEventsStore = { events: {}, order: [] };

export function edEventReducers(
    state: EDEventsStore = initialState,
    action: AnyAction,
): EDEventsStore {
    switch (action.type) {
        case SAVE_ED_EVENTS: {
            const events = (action as PayloadedAction<EDEvent[]>).payload;

            if (!events || events.length === 0) {
                return state;
            }

            return produce(state, (draft) => {
                events.forEach((evt) => {
                    draft.order.push(evt.id);
                    draft.events[evt.id] = evt;
                });
            });
        }
        default: {
            return state;
        }
    }
}
