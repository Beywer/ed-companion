import { EDEvent } from '../../../common/domain/EDEvent';
import { PayloadedAction } from '../../utils/actionTypes';

export const SAVE_ED_EVENTS = 'SAVE_ED_EVENTS';

export function saveEdEvents(events: EDEvent[]): PayloadedAction<EDEvent[]> {
    return { type: SAVE_ED_EVENTS, payload: events };
}
