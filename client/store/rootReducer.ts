import { combineReducers } from 'redux';
import { edEventReducers } from './edEvents/edEventReducers';
import {
    edEventsPath,
    locationPath,
    playerPath,
    scansByStarSystemPath,
    typesFilterPath,
} from './rootPaths';
import { typesFilterReducer } from './typesFilter/typesFilterReducers';
import { commanderReducer } from './gameData/commanderReducers';
import { locationReducer } from './gameData/locationReducers';
import { systemScanReducer } from './gameData/systemScanReducers';

export const rootReducer = combineReducers({
    [edEventsPath]: edEventReducers,
    [typesFilterPath]: typesFilterReducer,
    [playerPath]: commanderReducer,
    [locationPath]: locationReducer,
    [scansByStarSystemPath]: systemScanReducer,
});
