import { SystemScanStatus } from '../../domain/SystemScanStatus';
import { Action } from 'redux';
import { SAVE_ED_EVENTS } from '../edEvents/edEventActions';
import { PayloadedAction } from '../../utils/actionTypes';
import { EDEvent } from '../../../common/domain/EDEvent';
import produce from 'immer';
import { EDScanEvent, isScan } from '../../../common/domain/EDScanEvent';
import { SpaceBody } from '../../domain/SpaceBody';
import { SpaceBodyType } from '../../domain/SpaceBodyType';
import { ScanLevel } from '../../domain/ScanLevel';
import { EDScanType } from '../../../common/domain/EDScanType';
import { isFSSDiscoveryScan } from '../../../common/domain/EDFSSDiscoveryScanEvent';
import { isFSSAllBodiesFound } from '../../../common/domain/EDFSSAllBodiesFoundEvent';
import { isSAAScanComplete } from '../../../common/domain/EDSAAScanCompleteEvent';

type BySystemAddress = { [key: number]: SystemScanStatus };

const initialState: BySystemAddress = {};

export function systemScanReducer(
    state: BySystemAddress = initialState,
    action: Action,
): BySystemAddress {
    switch (action.type) {
        case SAVE_ED_EVENTS: {
            const { payload } = action as PayloadedAction<EDEvent[]>;

            return produce(state, (draft) => {
                payload.forEach((evt) => {
                    if (
                        isScan(evt) ||
                        isFSSDiscoveryScan(evt) ||
                        isFSSAllBodiesFound(evt) ||
                        isSAAScanComplete(evt)
                    ) {
                        const { SystemAddress } = evt;
                        draft[SystemAddress] =
                            draft[SystemAddress] || createScanInfo(SystemAddress);
                    }

                    if (isScan(evt)) {
                        const {
                            SystemAddress,
                            StarSystem,
                            BodyName,
                            WasMapped,
                            WasDiscovered,
                            ScanType,
                        } = evt;

                        draft[SystemAddress].name = StarSystem;

                        let body: SpaceBody | undefined = draft[SystemAddress].bodies.find(
                            (bd) => bd.name === BodyName,
                        );
                        if (!body) {
                            body = {
                                name: BodyName,
                                type: defineBodyType(evt),
                                mapped: WasMapped,
                                discovered: WasDiscovered,
                                scanLevel: ScanLevel.Unknown,
                            };
                            draft[SystemAddress].bodies.push(body);
                        }

                        updateBodyScanLevel(body, ScanType);

                        draft[SystemAddress].bodies.push();
                        return;
                    }

                    if (isFSSDiscoveryScan(evt)) {
                        const { SystemAddress, SystemName, BodyCount } = evt;
                        draft[SystemAddress].bodiesCount = BodyCount;
                        draft[SystemAddress].name = SystemName;
                    }
                    if (isFSSAllBodiesFound(evt)) {
                        const { SystemAddress, SystemName, Count } = evt;
                        draft[SystemAddress].bodiesCount = Count;
                        draft[SystemAddress].name = SystemName;
                    }

                    if (isSAAScanComplete(evt)) {
                        const { BodyName, SystemAddress } = evt;
                        const body = draft[SystemAddress].bodies.find((bd) => bd.name === BodyName);
                        if (body) {
                            body.scanLevel = ScanLevel.Mapped;
                        }
                    }
                });
            });
        }
        default: {
            return state;
        }
    }
}

function createScanInfo(systemAddress: number): SystemScanStatus {
    return {
        systemAddress,
        name: '',
        bodies: [],
        bodiesCount: 0,
    };
}

function updateBodyScanLevel(body: SpaceBody, scanType: EDScanType): void {
    switch (scanType) {
        case EDScanType.AutoScan: {
            if (body.scanLevel !== ScanLevel.Mapped && body.scanLevel !== ScanLevel.Detail) {
                body.scanLevel = ScanLevel.Discovered;
            }
        }
        case EDScanType.Detailed: {
            if (body.scanLevel !== ScanLevel.Mapped) {
                body.scanLevel = ScanLevel.Detail;
            }
        }
    }
}

function defineBodyType(evt: EDScanEvent): SpaceBodyType {
    const { StarType, PlanetClass } = evt;

    if (StarType) {
        return SpaceBodyType.Star;
    }

    if (PlanetClass) {
        return SpaceBodyType.Planet;
    }

    return SpaceBodyType.Belt;
}
