import { locationPath } from '../rootPaths';
import { Location } from '../../domain/Location';
import { EDBodyType } from '../../../common/domain/EDBodyType';

export function locationSelector(state: { [locationPath]?: Location }): Location {
    return (
        state[locationPath] || {
            systemAddress: 0,
            starSystem: '',
            body: '',
            bodyId: 0,
            bodyType: EDBodyType.Star,
        }
    );
}
