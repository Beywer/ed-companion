import { Action } from 'redux';
import { Location } from '../../domain/Location';
import { EDBodyType } from '../../../common/domain/EDBodyType';
import { SAVE_ED_EVENTS } from '../edEvents/edEventActions';
import produce from 'immer';
import { PayloadedAction } from '../../utils/actionTypes';
import { EDEvent } from '../../../common/domain/EDEvent';
import { isLocation } from '../../../common/domain/EDLocationEvent';
import { isFSDJump } from '../../../common/domain/EDFSDJumpEvent';

const initialState: Location = {
    systemAddress: 0,
    starSystem: '',
    body: '',
    bodyId: 0,
    bodyType: EDBodyType.Star,
};

export function locationReducer(state: Location = initialState, action: Action) {
    switch (action.type) {
        case SAVE_ED_EVENTS: {
            const { payload } = action as PayloadedAction<EDEvent[]>;

            return produce(state, (draft) => {
                payload.forEach((evt) => {
                    if (isLocation(evt) || isFSDJump(evt)) {
                        const { StarSystem, SystemAddress, Body, BodyID, BodyType } = evt;
                        draft.systemAddress = SystemAddress;
                        draft.starSystem = StarSystem;
                        draft.body = Body;
                        draft.bodyId = BodyID;
                        draft.bodyType = BodyType;
                    }
                });
            });
        }
        default: {
            return state;
        }
    }
}
