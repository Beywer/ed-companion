import { Commander } from '../../domain/Commander';
import { Action } from 'redux';
import { SAVE_ED_EVENTS } from '../edEvents/edEventActions';
import { PayloadedAction } from '../../utils/actionTypes';
import { EDEvent } from '../../../common/domain/EDEvent';
import produce from 'immer';
import { isLoadGame } from '../../../common/domain/EDLoadGameEvent';

const initialState: Commander = { name: '', ship: '', shipIdent: '', shipName: '' };

export function commanderReducer(state: Commander = initialState, action: Action) {
    switch (action.type) {
        case SAVE_ED_EVENTS: {
            const { payload } = action as PayloadedAction<EDEvent[]>;

            return produce(state, (draft) => {
                payload.forEach((evt) => {
                    if (isLoadGame(evt)) {
                        const { Commander, Ship_Localised, ShipName, ShipIdent } = evt;
                        draft.name = Commander;
                        draft.ship = Ship_Localised;
                        draft.shipName = ShipName;
                        draft.shipIdent = ShipIdent;
                    }
                });
            });
        }
        default: {
            return state;
        }
    }
}
