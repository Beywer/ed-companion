import { scansByStarSystemPath } from '../rootPaths';
import { SystemScanStatus } from '../../domain/SystemScanStatus';
import { createSelector } from 'reselect';
import { locationSelector } from './locationSelectors';

function scanStatusesSelector(state: {
    [scansByStarSystemPath]?: TypedObject<SystemScanStatus>;
}): TypedObject<SystemScanStatus> {
    return state[scansByStarSystemPath] || {};
}

export const currentSystemScanStatus = createSelector(
    [scanStatusesSelector, locationSelector],
    (scans, location) => {
        return scans[location.systemAddress] || { name: '', bodies: [] };
    },
);
