import { playerPath } from '../rootPaths';
import { Commander } from '../../domain/Commander';

export function commanderSelector(state: { [playerPath]?: Commander }): Commander {
    return state[playerPath] || { name: '', ship: '', shipName: '', shipIdent: '' };
}
