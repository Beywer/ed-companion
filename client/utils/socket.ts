import io from 'socket.io-client';
import { CONNECTION_TYPE } from '../../common/constants/socketEvents';

export function initSocket(): SocketIOClient.Socket {
    const socket = io.connect(localStorage.origin);
    socket.emit(CONNECTION_TYPE, 'client');
    return socket;
}
