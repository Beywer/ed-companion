import { Action, AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

export type GeneralThunkAction<R> = ThunkAction<R, GeneralObject, undefined, Action>;
export type GeneralThunkDispatch = ThunkDispatch<GeneralObject, undefined, Action>;

export type PayloadedAction<P> = AnyAction & { payload: P };
