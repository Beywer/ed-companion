export enum SpaceBodyType {
    Star = 'Star',
    Planet = 'Planet',
    Belt = 'Belt',
}
