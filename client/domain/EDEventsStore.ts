import { EDEvent } from '../../common/domain/EDEvent';

export interface EDEventsStore {
    events: TypedObject<EDEvent>;
    order: string[];
}
