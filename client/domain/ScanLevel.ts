export enum ScanLevel {
    Unknown = 'Unknown',
    Discovered = 'Discovered',
    Detail = 'Detail',
    Mapped = 'Mapped',
}
