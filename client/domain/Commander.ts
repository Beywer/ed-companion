export interface Commander {
    name: string;
    ship: string;
    shipName: string;
    shipIdent: string;
}
