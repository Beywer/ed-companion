import { ScanLevel } from './ScanLevel';
import { SpaceBodyType } from './SpaceBodyType';

export interface SpaceBody {
    name: string;
    type: SpaceBodyType;
    scanLevel: ScanLevel;
    discovered: boolean;
    mapped: boolean;
}
