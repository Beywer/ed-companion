import { EDBodyType } from '../../common/domain/EDBodyType';

export interface Location {
    systemAddress: number;
    starSystem: string;
    body: string;
    bodyId: number;
    bodyType: EDBodyType;
}
