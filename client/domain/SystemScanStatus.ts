import { SpaceBody } from './SpaceBody';

export interface SystemScanStatus {
    name: string;
    systemAddress: number;
    bodies: SpaceBody[];
    bodiesCount: number;
}
