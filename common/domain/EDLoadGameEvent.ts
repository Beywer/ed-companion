import { EDEvent } from './EDEvent';
import { EDGameModeType } from './EDGameModeType';
import { EDEventType } from './EDEventType';

export interface EDLoadGameEvent extends EDEvent {
    FID: string;
    Commander: string;
    Horizons: boolean;
    Ship: string;
    Ship_Localised: string;
    ShipID: number;
    ShipName: string;
    ShipIdent: string;
    FuelLevel: number;
    FuelCapacity: number;
    GameMode: EDGameModeType;
    Credits: number;
    Loan: number;
}

export function isLoadGame(event: EDEvent): event is EDLoadGameEvent {
    return event.event === EDEventType.LoadGame;
}
