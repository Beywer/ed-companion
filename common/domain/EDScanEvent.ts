import { EDEvent } from './EDEvent';
import { EDScanType } from './EDScanType';
import { EDStarType } from './EDStarType';
import { EDEventType } from './EDEventType';
import { EDPlanetClass } from './EDPlanetClass';

export interface EDScanEvent extends EDEvent {
    ScanType: EDScanType;
    BodyName: string;
    BodyID: number;
    Parents: any[];
    StarSystem: string;
    SystemAddress: number;
    DistanceFromArrivalLS: number;
    StarType?: EDStarType;
    PlanetClass?: EDPlanetClass;
    Subclass: number;
    StellarMass: number;
    Radius: number;
    AbsoluteMagnitude: number;
    Age_MY: number;
    SurfaceTemperature: number;
    Luminosity: string;
    SemiMajorAxis: number;
    Eccentricity: number;
    OrbitalInclination: number;
    Periapsis: number;
    OrbitalPeriod: number;
    RotationPeriod: number;
    AxialTilt: number;
    WasDiscovered: boolean;
    WasMapped: boolean;
}

export function isScan(evt: EDEvent): evt is EDScanEvent {
    return evt.event === EDEventType.Scan;
}
