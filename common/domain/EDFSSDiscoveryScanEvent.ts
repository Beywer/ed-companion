import { EDEvent } from './EDEvent';
import { EDEventType } from './EDEventType';

export interface EDFSSDiscoveryScanEvent extends EDEvent {
    Progress: number;
    BodyCount: number;
    NonBodyCount: number;
    SystemName: string;
    SystemAddress: number;
}

export function isFSSDiscoveryScan(evt: EDEvent): evt is EDFSSDiscoveryScanEvent {
    return evt.event === EDEventType.FSSDiscoveryScan;
}
