export enum EDStarType {
    O = 'O',
    B = 'B',
    A = 'A',
    F = 'F',
    G = 'G',
    K = 'K',
    M = 'M',
}
