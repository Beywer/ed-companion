import { EDEvent } from './EDEvent';
import { EDEventType } from './EDEventType';

export interface EDFSSAllBodiesFoundEvent extends EDEvent {
    SystemName: string;
    SystemAddress: 1183297148074;
    Count: number;
}

export function isFSSAllBodiesFound(evt: EDEvent): evt is EDFSSAllBodiesFoundEvent {
    return evt.event === EDEventType.FSSAllBodiesFound;
}
