import { EDEvent } from './EDEvent';
import { EDEventType } from './EDEventType';

export interface EDSAAScanCompleteEvent extends EDEvent {
    BodyName: string;
    SystemAddress: number;
    BodyID: number;
    ProbesUsed: number;
    EfficiencyTarget: number;
}

export function isSAAScanComplete(evt: EDEvent): evt is EDSAAScanCompleteEvent {
    return evt.event === EDEventType.SAAScanComplete;
}
