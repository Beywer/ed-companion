import { EDEvent } from './EDEvent';
import { EDStarPos } from './EDStarPos';
import { EDEventType } from './EDEventType';
import { EDBodyType } from './EDBodyType';

export interface EDLocationEvent extends EDEvent {
    Docked: boolean;
    StarSystem: string;
    SystemAddress: number;
    StarPos: EDStarPos;
    SystemAllegiance: string;
    SystemEconomy: string;
    SystemEconomy_Localised: string;
    SystemSecondEconomy: string;
    SystemSecondEconomy_Localised: string;
    SystemGovernment: string;
    SystemGovernment_Localised: string;
    SystemSecurity: string;
    SystemSecurity_Localised: string;
    Population: number;
    Body: string;
    BodyID: number;
    BodyType: EDBodyType;
}

export function isLocation(event: EDEvent): event is EDLocationEvent {
    return event.event === EDEventType.Location;
}
