import { EDEvent } from './EDEvent';
import { EDStarPos } from './EDStarPos';
import { EDBodyType } from './EDBodyType';
import { EDEventType } from './EDEventType';

export interface EDFSDJumpEvent extends EDEvent {
    StarSystem: string;
    SystemAddress: number;
    StarPos: EDStarPos;
    SystemAllegiance: string;
    SystemEconomy: string;
    SystemEconomy_Localised: string;
    SystemSecondEconomy: string;
    SystemSecondEconomy_Localised: string;
    SystemGovernment: string;
    SystemGovernment_Localised: string;
    SystemSecurity: string;
    SystemSecurity_Localised: string;
    Population: number;
    Body: string;
    BodyID: number;
    BodyType: EDBodyType;
    JumpDist: number;
    FuelUsed: number;
    FuelLevel: number;
}

export function isFSDJump(event: EDEvent): event is EDFSDJumpEvent {
    return event.event === EDEventType.FSDJump;
}
