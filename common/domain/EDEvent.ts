import { EDEventType } from './EDEventType';

export interface EDEvent {
    id: string;
    timestamp: string;
    event: EDEventType;
}
