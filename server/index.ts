import express from 'express';
import socketIo, { Socket } from 'socket.io';
import {
    CLIENT_NEW_EVENTS,
    CONNECTION_TYPE,
    LOCAL_READER_NEW_EVENTS,
} from '../common/constants/socketEvents';
import { EDEvent } from '../common/domain/EDEvent';
import path from 'path';
import { JournalsTail } from './src/EDJournals/JournalsTail';
import { JTail } from './src/EDJournals/JTail';
import { LocalReaderJTail } from './src/EDJournals/LocalReaderJTail';

const PORT = process.env.PORT || 3200;
const ENV = process.env.ENV || 'LOCAL';

console.log(
    Object.keys(process.env)
        .filter((k) => !k.startsWith('npm'))
        .reduce((acc, k) => ({ ...acc, [k]: process.env[k] }), {}),
);

const app = express();
app.use(express.static('server/dist'));

const indexPath = path.join(__dirname, 'dist/index.html');
['/', '/logs'].forEach((address) => {
    app.get(address, (req, res) => {
        res.sendFile(indexPath);
    });
});

const server = app.listen(PORT, () => console.log(`Server started at http://localhost:${PORT}`));

const io = socketIo(server);
console.log('socket.io connected to app');

const tail: JournalsTail = getJournalTail();

io.on('connection', function(socket: Socket) {
    socketLog(socket, 'connection');

    socket.on(CONNECTION_TYPE, (type: string, ack) => {
        socketLog(socket, CONNECTION_TYPE, type);
        switch (type) {
            case 'client': {
                handleClientConnection(socket);
                break;
            }
            case 'localReader': {
                handleLocalReaderConnection(socket);
                break;
            }
        }

        ack?.call();
    });
});

function handleClientConnection(socket: Socket): void {
    tail.on(sendEvents);
    socket.on('disconnect', () => {
        socketLog(socket, 'disconnect');
        tail.off(sendEvents);
    });

    function sendEvents(events: EDEvent[]): void {
        socket.emit(CLIENT_NEW_EVENTS, events);
    }
}

function handleLocalReaderConnection(socket: Socket): void {
    socket.on(LOCAL_READER_NEW_EVENTS, (events: EDEvent[]) => {
        socketLog(socket, LOCAL_READER_NEW_EVENTS, 'new events count', events.length);

        if (tail instanceof LocalReaderJTail) {
            tail.addEvents(events);
        }
    });

    socket.on('disconnect', () => {
        socketLog(socket, 'disconnect');
    });
}

function getJournalTail(): JournalsTail {
    if (ENV === 'HEROKU') {
        console.log('Create LocalReaderJTail');
        return new LocalReaderJTail();
    } else {
        console.log('Create JTail');
        return new JTail();
    }
}

function socketLog(socket: Socket, event: string, ...args: any[]): void {
    console.log(`Socket ${socket.id} ${event}${args.length > 0 ? ':' : ''}`, ...args);
}
