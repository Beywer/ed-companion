// @ts-ignore
import fs from 'fs';
import io from 'socket.io-client';
import { JTail } from './src/EDJournals/JTail';
import { EDEvent } from '../common/domain/EDEvent';
import { CONNECTION_TYPE, LOCAL_READER_NEW_EVENTS } from '../common/constants/socketEvents';

const configBuff = fs.readFileSync('./server/config.json');
const config = JSON.parse(configBuff.toString());

if (!config.appUrl) {
    throw new Error('appUrl have to be defined in config.json');
}

const socket = io.connect(config.appUrl);
const tail = new JTail();

socket.on('connect', function() {
    socket.emit(CONNECTION_TYPE, 'localReader', () => {
        tail.on((events: EDEvent[]) => {
            console.log('got events', events.length);
            socket.emit(LOCAL_READER_NEW_EVENTS, events);
        });
    });
});
