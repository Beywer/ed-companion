export function safeCall<T>(f: (...args: any[]) => T, ...args: any[]): T | null {
    try {
        return f(...args);
    } catch {
        return null;
    }
}
