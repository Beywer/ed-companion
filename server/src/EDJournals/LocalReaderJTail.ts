import { BaseJTail } from './BaseJTail';
import { EDEvent } from '../../../common/domain/EDEvent';

export class LocalReaderJTail extends BaseJTail {
    addEvents = (newEvents: EDEvent[]): void => {
        this.readEvents.push(...newEvents);
        this.notify(newEvents);
    };
}
