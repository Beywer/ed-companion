import { EDEvent } from '../../../common/domain/EDEvent';

/**
 * Monitor income events and report about them
 */
export interface JournalsTail {
    on: (handler: (events: EDEvent[]) => void) => void;
    off: (handler: (events: EDEvent[]) => void) => void;
}
