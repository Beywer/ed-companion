import { BaseJTail } from './BaseJTail';
import { JOURNALS_PATH } from './constants';
import os from 'os';
import { JournalsReader } from './JournalsReader';

/**
 * Reads ED logs from file system and notify about new events.
 */
export class JTail extends BaseJTail {
    private readonly reader: JournalsReader;
    private readTimeout: number | null = null;

    constructor() {
        super();

        const path = JOURNALS_PATH.replace('{username}', os.userInfo().username);
        this.reader = new JournalsReader(path);

        this.readNext();
    }

    private readNext = async () => {
        const events = await this.reader.readAllEvents();
        const newEvents = this.onlyNewEvents(events);
        if (newEvents.length > 0) {
            this.readEvents.push(...newEvents);
            this.notify(newEvents);
        }

        this.readTimeout = setTimeout(this.readNext);
    };
}
