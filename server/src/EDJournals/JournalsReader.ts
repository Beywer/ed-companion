import fs from 'fs';
import { EDEvent } from '../../../common/domain/EDEvent';
import { JOURNAL_REGEX } from './constants';

/**
 * Reads ED log files on specified path and return parsed EDEvent objects
 */
export class JournalsReader {
    private readonly path: string;
    private prevFilesCache: EDEvent[] | null = null;

    constructor(path: string) {
        this.path = path;
    }

    /**
     * Read all events from existing ED log files
     */
    public async readAllEvents(): Promise<EDEvent[]> {
        const files = await this.getJournalFiles();

        // First time read all log files, next times read only last
        // file and concat it's events with previous files events
        if (this.prevFilesCache === null) {
            return Promise.all(files.map((file) => this.parseJournalEvents(file))).then(
                (events) => {
                    // Array.flat not found O_O
                    const flat: EDEvent[] = [];
                    events.forEach((fileEvents, idx) => {
                        if (idx === events.length - 1) {
                            this.prevFilesCache = [...flat];
                        }
                        flat.push(...fileEvents);
                    });
                    return flat;
                },
            );
        } else {
            const lastFile = files[files.length - 1];
            return this.parseJournalEvents(lastFile).then((events: EDEvent[]) => {
                return [...(this.prevFilesCache ?? []), ...events];
            });
        }
    }

    /**
     * Defines all ED log files on configured path
     */
    private async getJournalFiles(): Promise<string[]> {
        const files = await fs.promises.readdir(this.path);
        return files.filter((name) => name.match(JOURNAL_REGEX)).sort();
    }

    /**
     * Read logs file and parse events
     */
    private async parseJournalEvents(journalName: string): Promise<EDEvent[]> {
        if (!journalName) {
            return Promise.resolve([]);
        }

        return fs.promises.readFile(`${this.path}/${journalName}`).then((buffer) => {
            return buffer
                .toString()
                .split('\n')
                .filter((str) => !!str)
                .map((str) => {
                    const evt: EDEvent = JSON.parse(str);
                    return evt;
                });
        });
    }
}
