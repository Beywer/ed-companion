import { JournalsTail } from './JournalsTail';
import { EDEvent } from '../../../common/domain/EDEvent';
import { safeCall } from '../utils/safeCall';

type Handler = (events: EDEvent[]) => void;

export class BaseJTail implements JournalsTail {
    protected handlers: Handler[] = [];
    protected readEvents: EDEvent[] = [];

    public on(handler: Handler): void {
        this.handlers.push(handler);
        safeCall(handler, this.readEvents);
    }
    public off(handler: Handler): void {
        this.handlers = this.handlers.filter((h) => h !== handler);
    }

    protected notify(newEvents: EDEvent[]): void {
        this.handlers.forEach((h) => safeCall(h, newEvents));
    }

    protected onlyNewEvents(nextEvents: EDEvent[]): EDEvent[] {
        if (nextEvents.length > this.readEvents.length) {
            return nextEvents.slice(this.readEvents.length);
        } else {
            return [];
        }
    }
}
