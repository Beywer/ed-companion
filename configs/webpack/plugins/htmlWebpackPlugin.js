const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = function() {
    return new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'client/index.html',
        // minify: true, // seems it doesn't work
    });
};
