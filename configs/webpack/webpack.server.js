const path = require('path');
const cwd = process.cwd();

const projectCode = require('./presets/projectCode');
const terserPlugin = require('./plugins/terserPlugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: {
        index: './server/index.ts',
        localReader: './server/localReader.ts',
    },
    output: {
        path: path.resolve(cwd, 'server'),
        filename: '[name].js',
    },

    module: {
        rules: [projectCode()],
    },

    plugins: [],

    resolve: {
        modules: ['node_modules', path.resolve(cwd, 'server')],
        extensions: ['.ts', '.js'],
    },

    target: 'node',
    externals: [nodeExternals()],

    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [terserPlugin()],
    },

    stats: {
        children: false,
    },
};
